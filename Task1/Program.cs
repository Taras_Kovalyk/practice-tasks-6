using System;

namespace PracticeTasks.Task1
{
    public class Program
    {
        public static void Main()
        {
            // Instantiation
            MatrixMinors equilateralMatrix = new MatrixMinors(new double[]{ 2, 98, 0, 89, 78 },
                                                              new double[]{ 67, 90, 78, 56, 87 },
                                                              new double[]{ 8, 8, 8, 9, 0 },
                                                              new double[]{ 57, 87, 9, 56, 0 },
                                                              new double[]{ 6, 2, 3, 1, 3 });

            MatrixMinors nonEquilateralMatrix = new MatrixMinors(new double[]{ 2, 98, 0 },
                                                                 new double[]{ 67, 90, 78 },
                                                                 new double[]{ 8, 8, 8 },
                                                                 new double[]{ 57, 87, 9 },
                                                                 new double[]{ 6, 2, 3 });
            
            MatrixMinors zeroMatrix = new MatrixMinors(5, 5);

            MatrixMinors emptyMatrix = new MatrixMinors();

            Console.WriteLine("=== Equilateral matrix");                                                                                                    
            PrintMatrix(equilateralMatrix.Matrix);
            Console.WriteLine();

            Console.WriteLine("=== Non-equilateral matrix");                                                                                                    
            PrintMatrix(nonEquilateralMatrix.Matrix);
            Console.WriteLine();
            
            // Minors demo
            Console.WriteLine("=== Minor determinant of order 2 for non-equilateral matrix.");
            Console.WriteLine("=== (works also for equilateral matrix)");
            Console.WriteLine(nonEquilateralMatrix.GetMinor(2));
            Console.WriteLine();

            Console.WriteLine("=== Complement minor determinant of order 3 for equilateral matrix.");
            Console.WriteLine("=== (InvalidOperationException for non-equilateral matrix)");           
            Console.WriteLine(equilateralMatrix.GetComplementaryMinor(3));
            Console.WriteLine();

            Console.WriteLine("=== (2,1) minor determinant for equilateral matrix.");
            Console.WriteLine("=== (InvalidOperationException for non-equilateral matrix)");           
            Console.WriteLine(equilateralMatrix.GetMinor(2, 1));
        }

        public static void PrintMatrix(double[,] matrix)
        {
            for(int i = 0; i < matrix.GetLength(0); ++i)
            {
                for(int j = 0; j < matrix.GetLength(1); ++j)
                {
                    Console.Write("{0,6}", matrix[i, j]);
                }
                Console.WriteLine();
            }

        }
    }
}