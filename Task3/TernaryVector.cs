using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace PracticeTasks.Task3
{
    /// <summary>
    /// Class implements ternary vector (only 0, 1, 2 values are allowed).
    /// </summary>
    public class TernaryVector : IEnumerable
    {
        #region Constructors
        public TernaryVector()
        {
            this.vector = new List<int>();
        }

        public TernaryVector(IEnumerable<int> items)
        {
            Validate(items);
            this.vector = items.ToList();
        }

        public TernaryVector(params int[] items)
        {
            Validate(items);
            vector = new List<int>();
            for (var i = 0; i < items.Length; ++i)
            {
                vector.Add(items[i]);
            }
        }
        #endregion

        #region Properties and indexator
        public List<int> vector { get; private set; }

        public int Count
        {
            get
            {
                return vector.Count;
            }
        }

        public int this[int index]
        {
            get
            {
                return vector[index];
            }
            set
            {
                Validate(new int[] { value });
                vector[index] = value;
            }
        }
        #endregion

        #region Overloaded operator and methods 
        /// <summary>
        /// Implementation of logical intersection.
        /// The logic is selecting the lowest of two items.
        /// </summary>
        public static TernaryVector operator +(TernaryVector me, TernaryVector other)
        {
            Validate(me, other);

            var res = new TernaryVector();

            for (var i = 0; i < me.Count; i++)
            {
                res.vector.Add(Math.Min(me[i], other[i]));
            }

            return res;
        }

        public static bool AreOrthogonal(TernaryVector first, TernaryVector second)
        {
            Validate(first, second);

            for (var i = 0; i < first.Count; ++i)
            {
                if (first[i] != 0 && second[i] != 0)
                {
                    return false;
                }
            }

            return true;
        }

        public int CountOf(int number)
        {
            Validate(new int[] { number });

            return this.vector.Count(item => item == number);
        }

        public IEnumerator GetEnumerator()
        {
            return this.vector.GetEnumerator();
        }

        /// <summary>
        /// Checking if specified array contains only allowed values.
        /// </summary>
        private static void Validate(IEnumerable<int> items)
        {
            foreach (var item in items)
            {
                if (item < 0 || item > 2)
                {
                    throw new ArgumentException("Number is out of allowed range.");
                }
            }
        }

        /// <summary>
        /// Checking if two vectors have the same length.
        /// </summary>
        private static void Validate(TernaryVector first, TernaryVector second)
        {
            if (first.Count != second.Count)
            {
                throw new InvalidOperationException("Vectors have different numbers of elements.");
            }
        }
        #endregion
    }
}